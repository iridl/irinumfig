Test
====

See :num:`figure #example-fig` on page :page:`example-fig` for the figure labeled :ref:`example-fig`.

.. _example-fig:

.. figure:: sample.*
   :scale: 50%

   Example figure


Hello, world!

See :num:`table #example-tab` on page :page:`example-tab` for the figure labeled :ref:`example-tab`.

.. _example-tab:

.. table:: Truth table for "not"

   =====  =====  ====== 
      Inputs     Output 
   ------------  ------ 
     A      B    A or B 
   =====  =====  ====== 
   False  False  False 
   True   False  True 
   False  True   True 
   True   True   True 
   =====  =====  ======

See :num:`table #example-tab2` on page :page:`example-tab2` for the figure labeled :ref:`example-tab2`.

.. _example-tab2:

.. table:: Truth table for "not" 2

   =====  =====  ====== 
      Inputs     Output 
   ------------  ------ 
     A      B    A or B 
   =====  =====  ====== 
   False  False  False 
   True   False  True 
   False  True   True 
   True   True   True 
   =====  =====  ======


A paragraph.
